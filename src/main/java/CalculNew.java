import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by User on 27.06.2017.
 */
public class CalculNew {
    public static WebDriver driver;

    @BeforeTest
    public void setup() {

        final File file = new File(PropertyLoader.loadProperty("path.webDriver"));
        System.setProperty(PropertyLoader.loadProperty("webDriver"), file.getAbsolutePath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


    @Test(priority = 1)
    public void openPage() {
        driver.navigate().to("http://juliemr.github.io/protractor-demo/");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.urlToBe("http://juliemr.github.io/protractor-demo/"));
    }

    @Test(priority = 2)
    public void additionTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("1");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "1");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("1");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "1");

        //selectOption
        CalcActions optionList = new CalcActions();
        optionList.selectOptionValue("ADDITION");
        optionList.getSelectionState(driver.findElement(By.xpath("/html/body/div/div/form/select/option[1]")));

    // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        CalcActions resultWaitMethod = new CalcActions();
        resultWaitMethod.waitForTextInResult("2");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "2");
    }
    @Test(priority = 3)
    public void divisionTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("10");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "10");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("5");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "5");

        //selectOption
        CalcActions optionList = new CalcActions();
        optionList.selectOptionValue("DIVISION");
        optionList.getSelectionState(driver.findElement(By.xpath("/html/body/div/div/form/select/option[2]")));

        // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        CalcActions resultWaitMethod = new CalcActions();
        resultWaitMethod.waitForTextInResult("2");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "2");
    }

    @Test(priority =4)
    public void moduloTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("10");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "10");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("3");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "3");

        //selectOption
        CalcActions optionList = new CalcActions();
        optionList.selectOptionValue("MODULO");
        optionList.getSelectionState(driver.findElement(By.xpath("/html/body/div/div/form/select/option[3]")));

        // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        CalcActions resultWaitMethod = new CalcActions();
        resultWaitMethod.waitForTextInResult("1");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "1");
    }

    @Test(priority =5)
    public void multiplicationTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("5");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "5");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("5");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "5");

        //selectOption
        CalcActions optionList = new CalcActions();
        optionList.selectOptionValue("MULTIPLICATION");
        optionList.getSelectionState(driver.findElement(By.xpath("/html/body/div/div/form/select/option[4]")));

        // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        CalcActions resultWaitMethod = new CalcActions();
        resultWaitMethod.waitForTextInResult("25");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "25");
    }

    @Test(priority =6)
    public void subtractionTest() {
        WebElement firstValue = driver.findElement(By.xpath("//input[@ng-model='first']"));
        firstValue.sendKeys("15");
        System.out.println(firstValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(firstValue.getAttribute("value"), "15");

        //input second value
        WebElement secondValue = driver.findElement(By.xpath("//input[@ng-model='second']"));
        secondValue.sendKeys("5");
        System.out.println(secondValue.getAttribute("value"));
        //validate the value in the second field
        assertEquals(secondValue.getAttribute("value"), "5");

        //selectOption
        CalcActions optionList = new CalcActions();
        optionList.selectOptionValue("SUBTRACTION");
        optionList.getSelectionState(driver.findElement(By.xpath("/html/body/div/div/form/select/option[5]")));

        // clickButton
        WebElement buttonGo = driver.findElement(By.xpath("//button[@ng-click='doAddition()']"));
        buttonGo.click();
        CalcActions resultWaitMethod = new CalcActions();
        resultWaitMethod.waitForTextInResult("10");
        // result value
        WebElement result = driver.findElement(By.xpath("//h2[@class]"));
        //assert result
        assertEquals(result.getText(), "10");
    }
}




