import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import java.io.File;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertTrue;

/**
 * Created by User on 01.07.2017.
 */
public class CalcActions extends CalculNew {

    public  WebElement operator = driver.findElement(By.xpath("//select[@ng-model='operator']"));

    public void selectOptionValue(String optionValue) {
        Select convertOptionToSelect = new Select(operator);
        convertOptionToSelect.selectByValue(optionValue);
    }

    public void getSelectionState(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        assertTrue(wait.until(ExpectedConditions.
                elementSelectionStateToBe((element), true)));
    }

    public void waitForTextInResult(String text){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//h2[@class]")),text));
    }

}
